import numpy as np
import scipy.io as scio

filename=input('Enter your filename: ')
output=input('Enter the output file name: ')

fileid=open(filename+'.mat','rb')
data=fileid.read(64)
rectype=np.dtype([('x', '<u4')]) 
coldata=np.fromfile(fileid,dtype=rectype,count=6)
(N,)=coldata[1]

rectype=np.dtype([('x', '<f')]) 
data=np.fromfile(fileid,dtype=rectype)

data_2=[x for x in data[1:-1]]
data_r=np.reshape(data_2,(N*(N+1)-1,2))
ind=np.setdiff1d(np.arange(0,N*(N+1)-1),np.arange(N,N*(N+1)-1,N+1))
data_r2=data_r[ind,:]

mat=np.array([complex(x,y) for (x,),(y,) in data_r2])
mat_r=np.reshape(mat,(N,N)).T

scio.savemat(output+'.mat',{output:mat_r, 'label':'impedance'}) 
